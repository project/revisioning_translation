/* 
 */

jQuery(document).ready(function() {
  
  jQuery('.revisioning_translation-mark-ajax').click(function() {
    jQuery.ajax({
      type: "GET",
      url: jQuery(this).attr('href'),
      //data: '',
      cache: false,
      dataType: 'json',
      success: function(msg) {
        jQuery(msg.id).children('.revisioning_translation-mark-ajax').hide().parent().children('.revisioning_translation-cancel-ajax').show();
      }
    });
    return false;
  });

  jQuery(".revisioning_translation-cancel-ajax").submit(function() {
    jQuery.ajax({
      type: "GET",
      url: jQuery(this).attr('action'),
      data: jQuery(this).serialize(),
      cache: false,
      dataType: 'json',
      success: function(msg) {
        jQuery(msg.id).children('.revisioning_translation-cancel-ajax').hide();
        jQuery(Drupal.settings.revisioning_translation_summary_latest_vid).children('.revisioning_translation-cancel-ajax').hide().parent().children('.revisioning_translation-mark-ajax').show();
        //jQuery(msg.id).children('.revisioning_translation-cancel-ajax').hide().parent().children('.revisioning_translation-mark-ajax').show();
      }
    });
    return false;
  });

//  Removed on client's suggestion
//  if (jQuery('#save_and_mark_for_translation_submit').is(':disabled')) {
//    var save_and_mark_for_translation_submit = false;
//  } else {
//    var save_and_mark_for_translation_submit = true;
//  }
//
//  jQuery('#edit-revision-moderation').change(function() {
//    if (jQuery(this).is(':checked') && save_and_mark_for_translation_submit == true) {
//      jQuery('#save_and_mark_for_translation_submit').removeAttr('disabled');
//    } else if (save_and_mark_for_translation_submit == true) {
//      jQuery('#save_and_mark_for_translation_submit').attr('disabled', 'disabled');
//    }
//  });
});